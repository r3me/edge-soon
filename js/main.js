//PreLoadMe
//$(window).on('load', function() { // makes sure the whole site is loaded 
//	$('#status').fadeOut(); // will first fade out the loading animation 
//	$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
//	$('body').delay(350).css({'overflow':'visible'});
//})

//GSAP
var tl = new TimelineLite();
var pre = $("#preloader");
var picture = $("picture");
var bg = $("picture img");
var h0 = $("h2, h3, h4, h5");
var header = $(".header");
var logo = $(".logo");

//TweenLite.to(pre, 0.5, {opacity:0}, 2);

tl.to(pre, 0.5, {opacity:0}, 0.5).to(pre, 0, {display:"none"})
.from(bg, 3, {x:100, ease:Power1.easeOut}, "-=1")
.from(picture, 1, {opacity:1}, "-=2")
.staggerFrom(h0, 1, {y:200, opacity:0, ease:Power1.easeOut}, 0.2, "-=1.5")
.from(logo, 1, {opacity:0}, "-=0.5");